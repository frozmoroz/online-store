<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AromaType extends Model
{
    protected $table = 'aroma_types';
}
