<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function volume() {
        return $this->belongsTo('App\Models\Volume', 'volume_id');
    }

    public function brand() {
        return $this->belongsTo('App\Models\Brand', 'brand_id');
    }

    public function concentration() {
        return $this->belongsTo('App\Models\Concentration', 'concentration_id');
    }

    public function aromaType() {
        return $this->belongsTo('App\Models\AromaType', 'aroma_type_id');
    }
}
