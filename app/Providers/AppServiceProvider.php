<?php

namespace App\Providers;

use App\Http\Controllers\SaleController;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\MenuController;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $view->with('menu', MenuController::index());
        });

        View::composer('welcome', function ($view) {
            $view->with('products', SaleController::index());
        });
    }
}
