<?php

namespace App\Providers;

use App\Services\CartActionsService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class CartActionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('CartActions', function()
        {
            return new CartActionsService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
