<?php

namespace App\Providers;

use App\Services\ProductActionsService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class ProductActionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('ProductActions', function()
        {
            return new ProductActionsService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
