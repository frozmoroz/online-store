<?php

namespace App\Providers;

use App\Services\GeneralActionsService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class GeneralActionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('GeneralActions', function()
        {
            return new GeneralActionsService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
