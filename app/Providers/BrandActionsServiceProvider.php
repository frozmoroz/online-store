<?php

namespace App\Providers;

use App\Services\BrandActionsService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;


class BrandActionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('BrandActions', function()
        {
            return new BrandActionsService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
