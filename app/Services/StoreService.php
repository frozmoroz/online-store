<?php


namespace App\Services;


use App\Facades\ProductActions;
use App\Models\AromaType;
use App\Models\Brand;
use App\Models\Concentration;
use App\Models\Product;
use App\Models\volume;

class StoreService
{
    public function storeContent($products, $searchInput = '')
    {
        $brands = Brand::get();
        $volumes = volume::get();
        $aromaTypes = AromaType::get();
        $concentrations = Concentration::get();
        ProductActions::setPriceDiscount($products);
        return view('store', ['title' => 'online-store', 'products' => $products, 'brands' => $brands, 'volumes' => $volumes, 'aromaTypes' => $aromaTypes, 'concentrations' => $concentrations, 'searchInput' => $searchInput]);
    }

    public function getSearchedProducts($request) {
        $searchInput = '%' . $request->searchInput . '%';

        $products = Product::select()->where('name', 'like', $searchInput)
            ->orWhere('description', 'like', $searchInput);

        $products = $products->orWhereHas('brand', function ($query) use ($searchInput) {
            $query->where('name', 'like', $searchInput);
        });
        $products = $products->orWhereHas('concentration', function ($query) use ($searchInput) {
            $query->where('name', 'like', $searchInput);
        });
        $products = $products->orWhereHas('AromaType', function ($query) use ($searchInput) {
            $query->where('name', 'like', $searchInput);
        });

        $products = $products->groupBy('product_group')->get();

        return $products;
    }

    public function filterByPrice($products, $request) {
        foreach ($products as $key => $product) {
            $priceFilter = $product->price;
            if ($product->discount > 0) {
                $priceFilter = $product->priceDiscount;
            }
            if ( (isset($request->priceStart) and ($priceFilter < $request->priceStart)) or (isset($request->priceEnd) and ($priceFilter > $request->priceEnd)) ) {
                unset($products[$key]);
            }
        }
        return $products;
    }
}