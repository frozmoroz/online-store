<?php


namespace App\Services;

use App\Facades\BrandActions;
use App\Facades\ProductActions;
use App\Models\Brand;

class BrandActionsService
{
    protected $brandList;

    public function __construct()
    {
        $this->brandList = new Brand();
    }

    public function show($slug) {
        $brand = $this->getBrandBySlug($slug);
        $products = ProductActions::getProductsByBrand($brand->id);
        $products = ProductActions::setPriceDiscount($products);

        return view('brands/index', ['title' => $brand->name, 'products' => $products, 'slug' => $slug]);
    }

    public function getBrands() {
        return $this->brandList::All();
    }

    public function getBrandBySlug($slug) {
        return $this->brandList::where('slug', $slug)->first();
    }

}