<?php


namespace App\Services;

use App\Facades\BrandActions;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class ProductActionsService
{
    protected $productList;

    public function __construct()
    {
        $this->productList = new Product();
    }

    public function show($brandSlug, $productSlug)
    {
        $product = $this->getValidProduct($brandSlug, $productSlug);
        $product = $this->setPriceDiscount($product);
        $productVolumes = $this->getVolumesByGroup($product->product_group);

        return view('product', ['title' => $product->name, 'product' => $product, 'productVolumes' => $productVolumes]);
    }

    public function getProducts() {
        return $this->productList::All();
    }

    public function getProductsByRequest($request) {
        $products = $this->productList::select()
            ->with('brand')
            ->with('concentration')
            ->with('AromaType')
            ->groupBy('product_group');

        if (isset($request->product_id)) {
            $products->whereIn('id', $request->product_id);
        }
        if (isset($request->brands)) {
            $products->whereIn('brand_id', $request->brands);
        }
        if (isset($request->volumes)) {
            $products->whereIn('volume_id', $request->volumes);
        }
        if (isset($request->concentrations)) {
            $products->whereIn('concentration_id', $request->concentrations);
        }
        if (isset($request->aromaTypes)) {
            $products->whereIn('aroma_type_id', $request->aromaTypes);
        }

        return $products->get();
    }

    public function getProductsByBrand($brandId)
    {
        return $this->productList::where('brand_id', '=', $brandId)
            ->with('brand')
            ->with('concentration')
            ->groupBy('product_group')
            ->get();
    }

    public function getProductBySlug($slug) {
        return Product::where('slug', $slug)
            ->with('brand')
            ->with('concentration')
            ->with('AromaType')
            ->groupBy('product_group')
            ->first();
    }

    public function getSales() {
        $products = Product::where('discount', '>', '0')
            ->with('brand')
            ->with('concentration')
            ->groupBy('product_group')
            ->get();

        $products = $this->setPriceDiscount($products);

        return $products;
    }

    public function getVolumesByGroup($productGroup) {
        $productVolumes = Product::where('product_group', '=', $productGroup)
            ->with('volume')
            ->orderBy('volume_id', 'ASC')
            ->get();

        $productVolumes = $this->setPriceDiscount($productVolumes);

        return $productVolumes;
    }

    public function getValidProduct($brandSlug, $productSlug) {

        $request = [
            'brand' => $brandSlug,
            'slug' => $productSlug
        ];

        $validator = Validator::make($request, [
            'brand' => 'required|alpha_dash',
            'slug' => 'required|alpha_dash'
        ]);

        if ($validator->fails()) {
            return abort(404);
        }

        $brand = BrandActions::getBrandBySlug($brandSlug);
        $product = $this->getProductBySlug($productSlug);

        if (empty($brand) or empty($product)) {
            return abort(404);
        }

        return $product;
    }

    public function setPriceDiscount($products)
    {
        if (isset($products->discount)) {
            if ($products->discount > 0) {
                $products->priceDiscount = $this->getPriceDiscount($products->price, $products->discount);
            }
            return $products;
        }

        foreach ($products as $product) {
            if ($product->discount > 0) {
                $product->priceDiscount = $this->getPriceDiscount($product->price, $product->discount);
            }
        }
        return $products;
    }

    public function getPriceDiscount($price, $discount)
    {
        $priceDiscount = $price / 100 * (100 - $discount);
        return $priceDiscount;
    }
}