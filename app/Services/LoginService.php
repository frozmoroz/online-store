<?php


namespace App\Services;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginService
{
    public function show() {
        if (Auth::check() && Auth::user()->is_admin) {
            return redirect('admin');
        } else if (Auth::check()) {
            return redirect(route('home'));
        }
        return view('login', ['title' => 'Авторизация']);
    }

    public function register($request) {
        $this->create($request);
        return response()->json([ 'title' => 'Регистрация', 'message' => 'Регистрация прошла успешно!' ]);
    }

    protected function create($data)
    {
        return User::create([
            'name' => $data->name,
            'email' => $data->email,
            'password' => Hash::make($data->password),
        ]);
    }

    public function signIn($request) {
        $credentials = array('email' => $request->email, 'password' => $request->password);

        if (Auth::attempt($credentials))
        {
            return ['signed' => true];
        } else {
            return response()->json([ 'title' => 'Ошибка авторизации', 'message' => 'Такого пользователя не существует' ], 400);
        }
    }
}