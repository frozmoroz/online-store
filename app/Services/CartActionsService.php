<?php


namespace App\Services;

use App\Models\Order;
use App\Models\OrderProduct;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartActionsService
{
    public function countPrice() {
        $subtotalPrice = Cart::subtotal(0,'','');
        $initialPrice = Cart::initial(0,'','');
        $result['differencePrice'] = number_format($initialPrice - $subtotalPrice, 2, '.', ' ');
        $result['subtotalPrice'] = Cart::subtotal(2,'.',' ');
        $result['initialPrice'] = Cart::initial(2,'.',' ');

        return $result;
    }

    public function addToCart($request) {
        Cart::add($request->id, $request->name, $request->productCount, $request->price, $request->volume,
            ['priceDiscount' => $request->priceDiscount, 'brand' => $request->brand, 'img' => $request->img]);

        $cart = Cart::content();
        $rowId = $cart->where('id', $request->id)->first()->rowId;

        Cart::setDiscount($rowId, $request->discount);
        return response()->json([ 'title' => 'Success', 'message' => 'Товар добавлен в корзину!' ]);
    }

    public function deleteFromCart($request) {
        if (isset($request->rowId)) {
            Cart::remove($request->rowId);
            $countPrice = $this->countPrice();
            $countPrice['countProducts'] = Cart::count();
            return $countPrice;
        } else {
            return response()->json([ 'title' => 'Ошибка', 'message' => 'Товар не найден!' ], 400);
        }
    }

    public function createOrder($request) {
        $orderId = Order::insertGetId([
            'name' => $request->name,
            'phone' => $request->phone,
            'products_count' => $request->productsCount,
            'initial_price' => $request->initialPrice,
            'difference_price' => $request->differencePrice,
            'subtotal_price' => $request->subtotalPrice,
        ]);
        foreach ($request->products_id as $product_id) {
            OrderProduct::insert(['order_id' => $orderId, 'product_id' => $product_id]);
        }

        return response()->json([ 'title' => 'Success', 'message' => 'Заказ оформлен!' ]);
    }
}