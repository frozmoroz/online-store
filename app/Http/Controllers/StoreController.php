<?php

namespace App\Http\Controllers;

use App\Facades\ProductActions;
use App\Facades\Store;
use App\Http\Requests\SearchStoreRequest;
use App\Http\Requests\StoreFilterRequest;

class StoreController extends Controller
{
    public function show()
    {
        $products = ProductActions::getProducts();
        $products = ProductActions::setPriceDiscount($products);
        return Store::storeContent($products);
    }


    public function filter(StoreFilterRequest $request)
    {
        $products = ProductActions::getProductsByRequest($request);
        $products = ProductActions::setPriceDiscount($products);
        $products = Store::filterByPrice($products, $request);
        $content = view('layouts.product.container', ['products' => $products, 'header' => 'В наличии'])->render();

        return response()->json(['content' => $content]);
    }

    public function searchStore(SearchStoreRequest $request)
    {
        $products = Store::getSearchedProducts($request);
        return Store::storeContent($products, $request->searchInput);
    }


}
