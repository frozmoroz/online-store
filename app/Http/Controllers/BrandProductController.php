<?php

namespace App\Http\Controllers;

use App\Facades\BrandActions;

class BrandProductController extends Controller
{
    public function show($slug)
    {
        return BrandActions::show($slug);
    }

}
