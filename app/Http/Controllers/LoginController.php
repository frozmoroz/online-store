<?php

namespace App\Http\Controllers;

use App\Facades\Login;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Alert;

class LoginController extends Controller
{
    public function show() {
        return Login::show();
    }

    public function register(RegisterRequest $request) {
        return Login::register($request);
    }

    public function signIn(LoginRequest $request) {
        return Login::signIn($request);
    }

    public function logout() {
        Auth::logout();
        return redirect('/login');
    }
}
