<?php

namespace App\Http\Controllers;

use App\Facades\ProductActions;
use App\Services\ProductService;

class SaleController extends Controller
{
    public static function index()
    {
        return ProductActions::getSales();
    }
}
