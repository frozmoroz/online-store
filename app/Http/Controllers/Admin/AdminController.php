<?php

namespace App\Http\Controllers\Admin;

use App\Facades\BrandActions;
use App\Facades\GeneralActions;
use App\Facades\ProductActions;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBrandRequest;
use App\Models\Brand;
use App\Models\Concentration;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\volume;
use App\Models\AromaType;

class AdminController extends Controller
{
    public function show()
    {
        return view('admin.index', ['title' => 'Админ панель']);
    }

    public function orders()
    {
        $orders = Order::All();
        return view('admin.orders', ['orders' => $orders, 'title' => 'Заказы']);
    }

    public function orderProducts($id)
    {
        $order = Order::where('id', $id)->first();
        $orderProducts = OrderProduct::where('order_id', $id)->pluck('product_id')->toArray();

        $products = Product::whereIn('id', $orderProducts)
            ->with('brand')
            ->with('concentration')
            ->with('AromaType')
            ->get();

        $products = ProductActions::setPriceDiscount($products);

        return view('admin.orderProducts', ['order' => $order, 'products' => $products, 'title' => 'Заказ № ' . $id]);
    }

    public function deliver($id) {
        $order = Order::where('id', $id)->first();
        $deliver = ($order->is_delivered == 1) ? 0 : 1;
        $order->is_delivered = $deliver;
        $order->save();
        return redirect(route('admin.orderProducts', $id));
    }

    public function addProducts() {
        $content['brands'] = Brand::get();
        $content['volumes'] = Volume::get();
        $content['aromaTypes'] = AromaType::get();
        $content['concentrations'] = Concentration::get();

        return view('admin.addProducts', ['content' => $content, 'title' => 'Добавление товаров']);
    }

    public function addBrands() {
        return view('admin.addBrands', ['title' => 'Добавление брэндов']);
    }

    public function createBrand(CreateBrandRequest $request) {
        $is_show = isset($request->is_show) ? 1 : 0;
        Brand::insert([
            'name' => $request->name,
            'is_show' => $is_show,
            'slug' => GeneralActions::createSlug($request->name),
        ]);
        return redirect('/admin/add-brands');
    }

    public function createProduct(CreateBrandRequest $request) {
        $is_show = isset($request->is_show) ? 1 : 0;
        Brand::insert([
            'name' => $request->name,
            'is_show' => $is_show,
            'slug' => GeneralActions::createSlug($request->name),
        ]);
        return redirect('/admin/add-brands');
    }
}
