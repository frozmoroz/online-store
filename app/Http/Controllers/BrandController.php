<?php

namespace App\Http\Controllers;

use App;
use App\Facades\BrandActions;

class BrandController extends Controller
{

    public function show()
    {
        return view('brands', ['title' => 'Brands', 'brands' => BrandActions::getBrands()]);
    }

}
