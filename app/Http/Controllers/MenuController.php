<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Brand;


class MenuController extends Controller
{
    public static function index() {
        $menu = Menu::where('is_show', '=', '1')->get();
        $brands = Brand::where('is_show', '=', '1')->get();
        $result = [];
        foreach ($menu as $item) {
            $result[$item->parent_id][] = $item;
        }
        foreach ($brands as $item) {
            $result['3'][] = $item;
        }

        return $result;
    }
}
