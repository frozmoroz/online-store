<?php

namespace App\Http\Controllers;

use App\Facades\CartActions;
use App\Http\Requests\AddToCartRequest;
use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Models\OrderProduct;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{

    public function show() {
        $cart = Cart::content();
        $countPrice = CartActions::countPrice();

        return view('cart', ['title' => 'Корзина', 'cart' => $cart, 'productsCount' => Cart::count(), 'initialPrice' => $countPrice['initialPrice'], 'subtotalPrice' => $countPrice['subtotalPrice'], 'differencePrice' => $countPrice['differencePrice']]);
    }

    public function addToCart(AddToCartRequest $request) {
        return CartActions::addToCart($request);
    }

    public function deleteFromCart(Request $request) {
        return CartActions::deleteFromCart($request);
    }

    public function destroyCart() {
        Cart::destroy();
    }

    public function createOrder(OrderRequest $request) {
        return CartActions::createOrder($request);
    }

}
