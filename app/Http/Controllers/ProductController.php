<?php

namespace App\Http\Controllers;

use App\Facades\ProductActions;
use App\Models\Brand;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function show($brand, $slug)
    {
        return ProductActions::show($brand, $slug);
    }

}
