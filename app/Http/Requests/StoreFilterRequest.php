<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brands' => 'array',
            'brands.*' => 'integer',
            'volumes' => 'array',
            'volumes.*' => 'integer',
            'concentrations' => 'array',
            'concentrations.*' => 'integer',
            'aromaTypes' => 'array',
            'aromaTypes.*' => 'integer',
            'priceStart' => 'numeric|nullable',
            'priceEnd' => 'numeric|nullable',
        ];
    }
}
