<?php


namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class ProductActions extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ProductActions';
    }
}