<?php


namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class GeneralActions extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'GeneralActions';
    }
}