<?php


namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class BrandActions extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'BrandActions';
    }
}