<?php


namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class CartActions extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'CartActions';
    }
}