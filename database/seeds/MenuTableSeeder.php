<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menuList[] = [
            'parent_id' => '0',
            'name' => 'ONLINE-МАГАЗИН',
        ];
        $menuList[] = [
            'parent_id' => '0',
            'name' => 'БРЕНДЫ',
        ];
        $menuList[] = [
            'parent_id' => '0',
            'name' => 'АКЦИИ',
        ];
        $menuList[] = [
            'parent_id' => '0',
            'name' => 'НОВОСТИ',
        ];
        $menuList[] = [
            'parent_id' => '0',
            'name' => 'МАГАЗИНЫ',
        ];


        DB::table('menu')->insert($menuList);
    }
}
