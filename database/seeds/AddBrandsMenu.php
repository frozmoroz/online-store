<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddBrandsMenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = DB::table('brands')
            ->get();

        foreach ($brands as $brand) {
            $brandList[] = [
                'parent_id' => '3',
                'name' => $brand->name,
                'is_show' => '1',
            ];
        }



        DB::table('menu')->insert($brandList);
    }
}
