<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Product;
use App\Facades\ProductActions;

class SLugProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $products = Product::where('slug', '=', '')->get();

        foreach ($products as $product) {
            Product::where('id', '=', $product->id)
                ->update([
                    "slug" => ProductActions::createSlug($product->name),
                ]);
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
