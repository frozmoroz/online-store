$('.js_searhButtonMenu').on("click", function () {
    $('#searchStore').submit();
});

$('.js_mainMenu li').on("mouseover", function () {
    var thisId = $(this).attr('data-id'),
        childMenu = $('.js_childMenu[data-parent=' + thisId + ']');

    showChild(childMenu);
});

$('.container').on("mouseover", function () {
    var childMenu = $('.js_childMenu');

    hideBrands(childMenu);
});

function showChild(childMenu) {
    childMenu.show();
}

function hideBrands(childMenu) {
    childMenu.hide();
}