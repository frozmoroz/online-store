$( document ).ready(function() {
    changeCartCount();
});

$('.js_productAddCart').on('click', function () {
    let productCount = $('#productCount').val();

    $('input[name="productCount"]').val(productCount);

    let form_data = $('#productForm').serializeArray();

    $.ajax({
        type: "POST",
        url: "/cart/add",
        data: form_data,
        dataType: 'json',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            changeCartCount();
            swalNotice(data);
        }
    });
});

function changeCartCount() {
    var cartText = $('.js_mainMenu').children('li:last').children('a');

    $.ajax({
        type: "POST",
        url: "/cart/count",
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            cartText.text('КОРЗИНА ('+ data +')');
        }
    });
}

$('.js_delCartBtn').on('click', function () {
    let rowId = $(this).data('id'),
        differencePrice = $('.js_differencePrice'),
        initialPrice = $('.js_initialPrice'),
        subtotalPrice = $('.js_subtotalPrice'),
        countProducts = $('.js_countProducts'),
        cartProductContainer = $(this).closest('.cartProductContainer');

    $.ajax({
        type: "POST",
        url: "/cart/deleteFromCart",
        data: {
            'rowId': rowId
        },
        dataType: 'json',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            changeCartCount();
            if (data['countProducts'] == 0) {
                location.reload();
            }
            differencePrice.text(data['differencePrice'] + ' руб.');
            initialPrice.text(data['initialPrice']  + ' руб.');
            subtotalPrice.text(data['subtotalPrice']  + ' руб.');
            countProducts.text(data['countProducts']);
            cartProductContainer.remove();
        },
        error: function (error) {
            swalNotice(error);
        }
    });
});

$('.js_cartClearBrn').on('click', function () {
    $.ajax({
        type: "POST",
        url: "/cart/destroyCart",
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function () {
            location.reload();
        }
    });
});

$('.js_cartTotalBtnOrder').on('click', function () {
    let form_data = $('#order').serializeArray();
    $.ajax({
        type: "POST",
        url: "/cart/createOrder",
        data: form_data,
        dataType: 'json',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            swalNotice(data);
            $('.js_cartTotalBtnOrder').addClass('cartTotalBtnOrderDisable');
        },
        error: function (error) {
            swalNotice(error);
        }
    });
});

