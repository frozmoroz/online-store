function swalNotice(notice) {
    if ((notice['responseJSON']) !== undefined) {
        notice = notice['responseJSON'];
    }

    if (notice['errors'] !== undefined) {
        notice['title'] = 'Ошибка';
        notice['message'] = '';

        $.each(notice['errors'], function( index, value ) {
            notice['message'] += value + "\n";
        });
    }

    swal({
        text: notice['message'],
        title: notice['title'],
    });
}

$('.js_showRegisterForm').on('click', function () {
    $('.js_RegisterForm').show();
    $('.js_signForm').hide();
});

$('.js_showSignForm').on('click', function () {
    $('.js_RegisterForm').hide();
    $('.js_signForm').show();
});

$('.js_register').on('click', function () {
    let form_data = $('#registerForm').serializeArray();
    $.ajax({
        type: "POST",
        url: "/register",
        data: form_data,
        dataType: 'json',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            swalNotice(data);
            setTimeout(function() {
                location.reload()
            }, 2000);

        },
        error: function (error) {
            swalNotice(error);
        }
    });
});

$('.js_signIn').on('click', function () {
    let form_data = $('#signForm').serializeArray();
    $.ajax({
        type: "POST",
        url: "/signIn",
        data: form_data,
        dataType: 'json',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            if (data['signed']) {
                window.location.replace('/admin');
            }
        },
        error: function (error) {
            swalNotice(error);
        }
    });
});
