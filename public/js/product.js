$('.productCardVolumeValue').on('click', function () {
    var priceOld = $(this).attr('data-price-old'),
        productId = $(this).attr('data-id'),
        productVolume = $(this).attr('data-value'),
        productDiscount = $(this).attr('data-discount'),
        priceDiscount = $(this).attr('data-discount-price'),
        productPrice = $(this).attr('data-price-old');

    if (productDiscount > 0) {
        $('.productPriceOld price').html(priceOld);
        $('.productPriceOld label').html('руб.');
        $('.discountPrice price').html(priceDiscount);
    } else {
        $('.productPriceOld price').html('');
        $('.productPriceOld label').html('');
        $('.discountPrice price').html(priceOld);
    }

    $('.productCardVolume').children().removeClass('selectedVolume');
    $(this).addClass('selectedVolume');

    $('input[name="id"]').val(productId);
    $('input[name="volume"]').val(productVolume);
    $('input[name="discount"]').val(productDiscount);
    $('input[name="price"]').val(productPrice);
    $('input[name="priceDiscount"]').val(priceDiscount);

})