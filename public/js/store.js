$('.js_storeFiltersApply').on('click', function () {
    let form_data = $('#filterForm').serializeArray();

    $.ajax({
        type: "POST",
        url: "/store/filter",
        data: form_data,
        dataType: 'json',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $('.productContainer').remove();
            $('.storeContent').append(data['content']);
            console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    });

});