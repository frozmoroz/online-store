@extends('layouts.app')
@section('title', $title)
@section('stylesheets')
    @parent
    <link href="{{asset('/css/product.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="productContainer">
        <div class="productCardContent">
            @if (!empty($product->priceDiscount))
                <div class="productSaleIcon">SALE</div>
            @endif
            <div class="productCardImg">
                <img src="{{asset(config('app.product_img_directory') . $product->img)}}">
            </div>
            <div class="productCardBar">
                <div class="productCardRow">
                    <div class="productCradRowContent">
                        <div class="productCardName">
                            <p class="productCardBrand">{{$product->brand->name}}</p>
                            <p class="productCardTitle">{{$product->name}}</p>
                            <p class="productCardConcentration">{{$product->concentration->name}}</p>
                        </div>
                        <div class="productCardPrice">
                            <div class="productCardPriceCount">Количество</div>
                            <div class="productCardPriceCountNumber"><input type="number" value="1" id="productCount"></div>
                            <div class="productCardPriceCost">
                                @if (!empty($product->priceDiscount))
                                    <div class="productPriceOld">
                                        <price>{{$product->price}}</price> <label>руб.</label>
                                    </div>
                                    <div class="discountPrice">
                                        <price>{{$product->priceDiscount}}</price> <label>руб.</label>
                                    </div>
                                @else
                                    <div class="productPriceOld">
                                        <price></price> <label></label>
                                    </div>
                                    <div class="discountPrice">
                                        <price>{{$product->price}}</price> <label>руб.</label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="productCardAction">
                            <div class="productAddCartButton js_productAddCart">
                                <span>Добавить в корзину</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="productCardRow">
                    <div class="productCardVolume">
                        <label>Объем</label>
                        @foreach($productVolumes as $productVolume)
                            @php $selectedVolume = ''; @endphp
                            @if ($productVolume->volume->id == $product->volume_id)
                                @php $selectedVolume = 'selectedVolume'; @endphp
                                @endif
                            <div data-id="{{$productVolume->id}}" data-value="{{$productVolume->volume->name}}" data-discount="{{$productVolume->discount}}" data-discount-price="{{$productVolume->priceDiscount}}" data-price-old="{{$productVolume->price}}" class="productCardVolumeValue {{$selectedVolume}}">
                                {{$productVolume->volume->name}} мл
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="productCardRow">
                    <div class="productCardDescription">
                        <p>Описание</p>
                        {{$product->description}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::open(['id'=>'productForm', 'method' => 'post', 'action' => 'CartController@addToCart' ]) !!}
    {!! Form::hidden('id', $product->id)  !!}
    {!! Form::hidden('brand', $product->brand->name)  !!}
    {!! Form::hidden('name', $product->name) !!}
    {!! Form::hidden('img', $product->img) !!}
    {!! Form::hidden('productCount', '1') !!}
    {!! Form::hidden('price', $product->price) !!}
    {!! Form::hidden('volume', $product->volume->name) !!}
    {!! Form::hidden('discount', $product->discount) !!}
    {!! Form::hidden('priceDiscount', $product->priceDiscount) !!}
    {!! Form::close() !!}
@endsection
@section('scripts')
    @parent
    <script src="{{asset('/js/product.js')}}"></script>
    @endsection


