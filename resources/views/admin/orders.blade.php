@extends('layouts.app')
@section('title', $title)
@section('stylesheets')
    @parent
    <link href="{{asset('/css/admin.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
        <div class="ordersContainer">
            <div class="ordersContent">
                <div class="ordersTitle">
                    Заказы
                </div>
                <div class="ordersTable">
                    <div class="ordersRow orderRowTitle">
                        <div class="ordersCol ordersColId">
                            ID
                        </div>
                        <div class="ordersCol ordersColName">
                            Имя
                        </div>
                        <div class="ordersCol ordersColPhone">
                            Телефон
                        </div>
                        <div class="ordersCol ordersColCount">
                            Кол-во
                        </div>
                        <div class="ordersCol ordersColPrice">
                            Цена без скидки
                        </div>
                        <div class="ordersCol ordersColDiscount">
                            Сумма скидки
                        </div>
                        <div class="ordersCol ordersColItog">
                            Итоговая сумма
                        </div>
                    </div>
                    @foreach($orders as $order)
                        @php if ($order->is_delivered) {$delivered = 'delivered';} else {$delivered = '';} @endphp
                        <div class="ordersRow js_showOrderProducts {{$delivered}}" data-id="{{$order->id}}">
                            <div class="ordersCol ordersColId">
                                {{$order->id}}
                            </div>
                            <div class="ordersCol ordersColName">
                                {{$order->name}}
                            </div>
                            <div class="ordersCol ordersColPhone">
                                {{$order->phone}}
                            </div>
                            <div class="ordersCol ordersColCount">
                                {{$order->products_count}}
                            </div>
                            <div class="ordersCol ordersColPrice">
                                {{$order->initial_price}} р.
                            </div>
                            <div class="ordersCol ordersColDiscount">
                                {{$order->difference_price}} р.
                            </div>
                            <div class="ordersCol ordersColItog">
                                {{$order->subtotal_price}} р.
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>

@endsection

@section('scripts')
    @parent
    <script src="{{asset('/js/admin.js')}}"></script>
    @endsection