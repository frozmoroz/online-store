@extends('layouts.app')
@section('title', $title)
@section('stylesheets')
    @parent
    <link href="{{asset('/css/admin.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="adminPanelContainer">
        <div class="adminPanelContent">
            <div class="adminMenu">
                <div class="adminMenuItem">
                    <a href="{{asset(route('admin.addBrands'))}}">Добавить брэнд</a>
                </div>
                <div class="adminMenuItem">
                    <a href="{{asset(route('admin.addProducts'))}}">Добавить товар</a>
                </div>
                <div class="adminMenuItem">
                    <a href="{{asset(route('admin.orders'))}}">Заказы</a>
                </div>
                <div class="adminMenuItem">
                    <a href="{{asset(route('logout'))}}">Выход</a>
                </div>
            </div>
        </div>
    </div>
@endsection