@extends('layouts.app')
@section('title', $title)
@section('stylesheets')
    @parent
    <link href="{{asset('/css/admin.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')

    <div class="addToTableContainer">
        <div class="addToTableContent">
            <div class="addBlock">
                {!! Form::open(['id'=>'addBrand', 'method' => 'post', 'action' => 'Admin\AdminController@createBrand']) !!}
                <input class="addInput" name="name" type="text" placeholder="Введите наименование">
                Показывать <input type="checkbox" name="is_show" value="1">

                <div class="loginButtons">
                    <div class="loginBtnLeft js_addToTable">
                        <span>Добавить</span>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script src="{{asset('/js/admin.js')}}"></script>
@endsection