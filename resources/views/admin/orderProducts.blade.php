@extends('layouts.app')
@section('title', $title)
@section('stylesheets')
    @parent
    <link href="{{asset('/css/admin.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="orderProductContainer">
        <div class="orderProductContent">

            <div class="orderProductDelivered">
                @if ($order->is_delivered)
                    <a href="{{asset(route('admin.deliver', $order->id))}}">
                    <div class="orderProductDeliveredBtn deliveredBtn js_deliver">
                        <span>Доставлен</span>
                    </div>
                    </a>
                @else
                    <a href="{{asset(route('admin.deliver', $order->id))}}">
                    <div class="orderProductDeliveredBtn">
                        <span>Не доставлен</span>
                    </div>
                    </a>
                @endif
            </div>

            <div class="orderProductTotalContent">
                <div class="orderProductTotalTitle">
                    Заказ <span>№ {{$order->id}}</span>
                </div>
                <div class="orderProductTotalCount">
                    Товаров: <span>{{$order->products_count}}</span>
                </div>
                <div class="orderProductInitialPrice">
                    На сумму: <span>{{$order->initial_price}} руб.</span>
                </div>
                <div class="orderProductDifferencePrice">
                    Величина скидки: <span>{{$order->difference_price}} руб.</span>
                </div>
                <div class="orderProductTotalPrice">
                    Итого: <span>{{$order->subtotal_price}} руб.</span>
                </div>
            </div>


            @include('layouts.product.container', ['products' => $products, 'header' => 'Товары в заказе '])
        </div>
    </div>
    <div class="footerPush"></div>
@endsection

@section('scripts')
    @parent
    <script src="{{asset('/js/admin.js')}}"></script>
@endsection