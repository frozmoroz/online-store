@extends('layouts.app')
@section('title', $title)
@section('stylesheets')
    @parent
    <link href="{{asset('/css/admin.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')

    <div class="addToTableContainer">
        <div class="addToTableContent">
            <div class="addBlock">
                {!! Form::open(['id'=>'addBrand', 'method' => 'post', 'action' => 'Admin\AdminController@createBrand']) !!}
                <select name="brand_id" class="addInput">
                    @foreach($content['brands'] as $brand)
                    <option>{{$brand->name}}</option>
                        @endforeach
                </select>
                <select name="volume_id" class="addInput">
                    @foreach($content['volumes'] as $volume)
                        <option>{{$volume->name}}</option>
                    @endforeach
                </select>
                <select name="concentration_id" class="addInput">
                    @foreach($content['concentrations'] as $concentration)
                        <option>{{$concentration->name}}</option>
                    @endforeach
                </select>
                <select name="aroma_type_id" class="addInput">
                    @foreach($content['aromaTypes'] as $aromaType)
                        <option>{{$aromaType->name}}</option>
                    @endforeach
                </select>
                <input class="addInput" name="name" type="text" placeholder="Введите наименование">
                <input class="addInput" name="price" type="text" placeholder="Введите цену">
                <input class="addInput" name="discount" type="text" placeholder="Введите скидку">
                Показывать <input type="checkbox" name="is_show" value="1">

                <div class="loginButtons">
                    <div class="loginBtnLeft js_addToTable">
                        <span>Добавить</span>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    @parent
    <script src="{{asset('/js/admin.js')}}"></script>
@endsection