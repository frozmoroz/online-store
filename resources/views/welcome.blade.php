@extends('layouts.app')
@section('title', $title)
@section('stylesheets')
    @parent
    <link href="{{asset('/css/slider.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/css/sales.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
        @include('layouts.slider.slider')
        @include('layouts.sales')
    <div class="footerPush"></div>
@endsection
@section('scripts')
    @parent
    <script src="{{asset('/js/slider.js')}}"></script>
@endsection


