<div class="footer">
    <div class="footerContent">
        <div class="footerColumn">
            КЛИЕНТАМ<br>
            Контакты<br>
            Стать клиентом<br>
            Акции<br>
            Доставка<br>
            Оплата<br>
            Оплата частями<br>
            Обмен и возврат<br>
            Вопрос-ответ<br>
        </div>
        <div class="footerColumn">
            О КОМПАНИИ<br>
            О компании<br>
            Магазины<br>
            Бренды<br>
            Дисконтная программа<br>
            Мероприятия<br>
            Партнеры<br>
            Контакты<br>
            Карта сайта<br>
        </div>
        <div class="footerColumn">
            СЕРВИСЫ<br>
            Обратная связь<br>
            Подписки<br>
            Look Modeler<br>
        </div>
        <div class="footerColumn">
            О ПРОЕКТЕ<br>
            Пользовательское соглашение<br>
            Обработка персональных данных<br>
            Правила модерации<br>
        </div>

    </div>
</div>