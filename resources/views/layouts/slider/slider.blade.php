<div class="slider">
    <div class="item">
        <img src="img/slider/slide1.jpg" alt="Первый слайд">
        {{--<div class="slideText">Заголовок слайда 1</div>--}}
    </div>

    <div class="item">
        <img src="img/slider/slide2.jpg" alt="Второй слайд">
    </div>

    <div class="item">
        <img src="img/slider/slide3.jpg" alt="Третий слайд">
    </div>

    <a class="prev" onclick="minusSlide()">&#10094;</a>
    <a class="next" onclick="plusSlide()">&#10095;</a>
</div>
<br>

<div class="slider-dots">
    <span class="slider-dots_item" onclick="currentSlide(1)"></span>
    <span class="slider-dots_item" onclick="currentSlide(2)"></span>
    <span class="slider-dots_item" onclick="currentSlide(3)"></span>
</div>