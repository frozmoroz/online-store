@php if (!isset($searchInput)) $searchInput = ''; @endphp
<div class="navMenu">
    <div class="upperBarMenu">
        <div class="contentMenu">
            @foreach($menu as $idMenu => $itemMenu)
                @if ($idMenu == 0)
                    <ul class="listMenu js_mainMenu">
                        @foreach($itemMenu as $item)
                            <li data-id=" {{$item->id}} ">
                                @if (!empty($item->slug))
                                    <a href="{{ url($item->slug) }}">
                                        {{$item->name}}
                                    </a>
                                @else
                                    {{$item->name}}
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @else
                    <div class="dropDownMenu js_childMenu" data-parent="{{$idMenu}}">
                        <ul>
                            @foreach($itemMenu as $item)
                                <li>
                                    @if (!empty($item->slug))
                                        <a href="{{route('products', $item->slug)}}">
                                            {{$item->name}}
                                        </a>
                                    @else
                                        {{$item->name}}
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                        @endif
                        @endforeach
                    </div>
        </div>
    </div>
    <div class="lowerBarMenu">
        <div class="contentMenu lowerBarMenuColumns">
            <div class="lowerBarColumn titleMenu">
                <a href="/"> Online-shop </a>
            </div>
            <div class="lowerBarColumn searchLineMenu">
                {!! Form::open(['id'=>'searchStore', 'method' => 'post', 'action' => 'StoreController@searchStore' ]) !!}
                <img src="{{asset('/img/icons/searchMenuIcon.png')}}"  class="searhButtonMenu js_searhButtonMenu" >
                <input type="text" name="searchInput" value="{{$searchInput}}" class="searchQueryMenu">
                {!! Form::close() !!}
            </div>
            <div class="lowerBarColumn contactsMenu">
                Звоните с 10 до 21<br>
                +7961232281
            </div>
        </div>
    </div>
</div>