<html lang="{{ config('app.locale') }}">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

@section('stylesheets')
    @include('layouts.stylesheets.stylesheets')
@show

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
</head>

<body>
@section('menu')
    @include('layouts.menu.menu')
@show

<div class="container">
    <div class="content">
        @include('layouts.banner')
        @yield('content')
    </div>
</div>
@include('layouts.footer')
@section('scripts')
    @include('layouts.scripts.scripts')
@show

</body>
</html>