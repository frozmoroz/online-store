<div class="productContainer">
    <div class="productHeader">
        @if (@count($products) == 0)
            Товары не найдены
        @else
            {{$header}}
        @endif
    </div>
    @foreach($products as $product)
        <div class="productBlock">
            <a href="{{asset('brands/' .$product->brand->slug . '/' . $product->slug)}}">
                <div class="productContent">
                    <div class="productImg">
                        <img src="{{  asset(config('app.product_img_directory') . $product->img) }}">
                    </div>
                    <div class="productBrand">
                        {{$product->brand->name}}
                    </div>
                    <div class="productName">
                        {{$product->name}}
                    </div>
                    <div class="productConcentration">
                        {{$product->concentration->name}}
                    </div>
                    @if (!empty($product->priceDiscount))
                        <div class="productPriceOld">
                            {{$product->price}}
                            <label>руб.</label>
                        </div>
                        <div class="productPrice">
                            {{$product->priceDiscount}}
                            <label>руб.</label>
                        </div>
                    @else
                        <div class="productPrice">
                            {{$product->price}}
                            <label>руб.</label>
                        </div>
                    @endif

                </div>
            </a>
        </div>
    @endforeach
</div>