<div class="salesContainer">
    <div class="salesHeader">
        Sales
    </div>
    @foreach($products as $product)
        <div class="salesProduct">
            <a href="{{'brands/' . $product->brand->slug . '/' . $product->slug}}">
            <div class="salesProductContent">
                <div class="salesImg">
                    <img src="{{ asset(config('app.product_img_directory') . $product->img) }}">
                </div>
                <div class="salesBrand">
                    {{$product->brand->name}}
                </div>
                <div class="salesName">
                    {{$product->name}}
                </div>
                <div class="salesConcentration">
                    {{$product->concentration->name}}
                </div>
                <div class="salesPriceOld">
                    {{$product->price}}
                    <label>руб.</label>
                </div>
                <div class="salesPrice">
                    {{$product->priceDiscount}}
                    <label>руб.</label>
                </div>
            </div>
            </a>
        </div>
    @endforeach
</div>