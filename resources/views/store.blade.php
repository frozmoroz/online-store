@extends('layouts.app')
@section('title', $title)
@section('stylesheets')
    @parent
    <link href="{{asset('/css/store.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="storeContainer">
        <div class="storeFiltersConteiner">
            <form method="post" action="#" id="filterForm">
            <div class="storeFiltersBrands storeFilter">
                <div class="storeFiltersName">
                    Брэнд
                </div>
                <div class="storeFiltersContent">
                    <div class="storeFiltersValues">
                        @foreach($brands as $brand)
                            <div class="storeFiltersItem"><input type="checkbox" name="brands[]" value="{{$brand->id}}">
                                <label>{{$brand->name}}</label></div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="storeFiltersAroma storeFilter">
                <div class="storeFiltersName">
                    Аромат
                </div>
                <div class="storeFiltersContent">
                    <div class="storeFiltersValues">
                        @foreach($aromaTypes as $type)
                            <div class="storeFiltersItem"><input type="checkbox" name="aromaTypes[]" value="{{$type->id}}">
                                <label>{{$type->name}}</label></div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="storeFiltersConcentration storeFilter">
                <div class="storeFiltersName">
                    концентрация
                </div>
                <div class="storeFiltersContent">
                    <div class="storeFiltersValues">
                        @foreach($concentrations as $concentration)
                            <div class="storeFiltersItem"><input type="checkbox" name="concentrations[]" value="{{$concentration->id}}">
                                <label>{{$concentration->name}}</label></div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="storeFiltersVolume storeFilter">
                <div class="storeFiltersName">
                    Объем
                </div>
                <div class="storeFiltersContent">
                    <div class="storeFiltersValues">
                        @foreach($volumes as $volume)
                            <div class="storeFiltersItem"><input type="checkbox" name="volumes[]" value="{{$volume->id}}">
                                <label>{{$volume->name}} мл</label></div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="storeFiltersPrice storeFilter">
                <div class="storeFiltersName">
                    Цена
                </div>
                <div class="storeFiltersContent">
                    от <input type="number" class="storeFiltersPriceRange" name="priceStart">
                    до <input type="number" class="storeFiltersPriceRange" name="priceEnd">
                </div>
            </div>
            <div class="storeFiltersApply js_storeFiltersApply">
                <span>Применить</span>
            </div>
            </form>
        </div>
        <div class="storeContent">
            @include('layouts.product.container', ['products' => $products, 'header' => 'В наличии'])
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{asset('/js/store.js')}}"></script>
@endsection
