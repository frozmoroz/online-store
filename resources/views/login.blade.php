@extends('layouts.app')
@section('title', $title)
@section('content')
    <div class="loginContainer">
        <div class="loginContent">
            <div class="loginBlock js_signForm">
                <div class="loginBlockContent">
                <div class="loginTitle">
                    Вход
                </div>
                    {!! Form::open(['id'=>'signForm', 'method' => 'post' ]) !!}
                    <div class="loginEmail">
                        <input name="email" type="email" placeholder="Введите E-mail">
                    </div>
                    <div class="loginPassword">
                        <input name="password" type="password" placeholder="Введите пароль">
                    </div>
                    <div class="loginButtons">
                            <div class="loginBtnLeft js_signIn">
                                <span>Войти</span>
                            </div>
                            <div class="loginBtnRight js_showRegisterForm">
                                <span>Я еще не зарегистрирован</span>
                            </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
                <div class="RegisterBlock js_registerForm">
                    <div class="loginBlockContent">
                        <div class="loginTitle">
                            Регистрация
                        </div>
                        {!! Form::open(['id'=>'registerForm', 'method' => 'post' ]) !!}
                        <div class="RegisterName">
                            <input name="name" type="text" placeholder="Введите имя">
                        </div>
                        <div class="RegisterEmail">
                            <input name="email" type="email" placeholder="Введите E-mail">
                        </div>
                        <div class="RegisterPassword">
                            <input name="password" type="password" placeholder="Введите пароль">
                        </div>
                        <div class="RegisterPassword">
                            <input name="password_confirmation" type="password" placeholder="Пароль еще раз">
                        </div>
                        <div class="loginButtons">
                                <div class="RegisterBtnLogin js_register">
                                    <span>Зарегистрироваться</span>
                                </div>
                                <div class="RegisterBtn js_showSignForm">
                                    <span>Я уже зарегистрирован</span>
                                </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
            </div>

    </div>
    </div>
@endsection