@extends('layouts.app')
@section('title', $title)
@section('content')
    <div class="brandsContainer">
        @foreach($brands as $brand)
            <div class="brandColumn">
                <a href="{{route('products', $brand->slug)}}"> {{$brand->name}} </a>
            </div>
        @endforeach

    </div>
@endsection


