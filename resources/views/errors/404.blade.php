@extends('layouts.app')
@section('title', 'Ошибка')

@section('content')
    <div class="errorPageContainer">
        <div class="errorPageContent">
            <img src="{{ asset('img/404.png') }}">
            <div class="errorPageTitle">
                <label>404</label>
                <p><span>УПС!</span> кажется что то пошло не так</p>
                <p>Страница не найдена</p>
            </div>
        </div>
    </div>
@endsection