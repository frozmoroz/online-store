@extends('layouts.app')
@section('title', $title)
@section('stylesheets')
    @parent
    <link href="{{asset('/css/cart.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')

    <div class="cartContainer">
        <div class="cartContent">
            @if (count($cart) == 0)
                <div class="cartTitle">
                    Корзина пуста
                </div>
                @else
                {!! Form::open(['id'=>'order', 'method' => 'post', 'action' => 'CartController@createOrder' ]) !!}
            <div class="cartTitle">
                Корзина
            </div>
            <div class="cartProductTitle">
                <div class="cartProductTitleName">
                    Наименование
                </div>
                <div class="cartProductTitleCount">
                    Количество
                </div>
                <div class="cartProductTitlePrice">
                    Цена
                </div>
            </div>
            @foreach($cart as $product)
                <div class="cartProductContainer">
                    <div class="cartProductContent">
                    <div class="delCartProduct">
                        <div class="delCartBtn js_delCartBtn" data-id="{{$product->rowId}}">
                            &#10008;
                        </div>
                    </div>
                    <div class="cartImgProduct">
                        <img src="{{asset(config('app.product_img_directory') . $product->options->img)}}">
                    </div>
                    <div class="cartDescription">
                        <div class="cartNameProduct">
                            {{$product->name}}
                        </div>
                        <div class="cartBrandProduct">
                            {{$product->options->brand}}
                        </div>
                        <div class="cartVolumeProduct">
                            объем:  {{$product->weight}} мл
                        </div>
                    </div>
                    <div class="cartCountProduct">
                        {{$product->qty}}
                    </div>
                    <div class="cartPriceProduct">
                        @if (is_null($product->options->priceDiscount))
                            <div class="cartPriceSumm">
                                {{$product->qty}} x  {{$product->price}} =
                            </div>
                            <div class="cartPriceTotal">
                                {{$product->price * $product->qty}} руб.
                            </div>
                        @else
                            <div class="cartPriceSumm">
                                {{$product->qty}} x  {{$product->options->priceDiscount}} =
                            </div>
                            <div class="cartPriceTotal">
                                {{$product->options->priceDiscount * $product->qty}} руб.
                            </div>
                            <div class="cartPriceTotalOld">
                                {{$product->price * $product->qty}} руб.
                            </div>
                        @endif
                    </div>
                    </div>
                </div>
                    {!! Form::hidden('products_id[]', $product->id) !!}
            @endforeach


            <div class="cartTotalContainer">
            <div class="cartTotalContent">
                <div class="cartTotalTitle">
                    Ваш заказ
                </div>
                <div class="cartTotalCount">
                    Товаров: <span class="js_countProducts">{{$productsCount}}</span>
                    {!! Form::hidden('productsCount', $productsCount) !!}
                </div>
                <div class="cartInitialPrice">
                    На сумму: <span class="js_initialPrice">{{$initialPrice}} руб.</span>
                    {!! Form::hidden('initialPrice', $initialPrice) !!}
                </div>
                <div class="cartDifferencePrice">
                   Величина скидки: <span class="js_differencePrice">{{$differencePrice}} руб.</span>
                    {!! Form::hidden('differencePrice', $differencePrice) !!}
                </div>
                <div class="cartTotalPrice">
                    Итого: <span class="js_subtotalPrice">{{$subtotalPrice}} руб.</span>
                    {!! Form::hidden('subtotalPrice', $subtotalPrice) !!}
                </div>

                <div class="cartTotalOrderName">
                    <input name="name" type="text" placeholder="Введите имя">
                </div>
                <div class="cartTotalOrderPhone">
                    <input name="phone" type="text" placeholder="Введите телефон">
                </div>
                <div class="cartOrderNotice">

                </div>
                <div class="cartTotalOrder">
                    <div class="cartTotalBtnOrder js_cartTotalBtnOrder">
                        <label>Оформить заказ</label>
                    </div>
                </div>
            </div>
                <div class="cartClear">
                    <div class="cartClearBrn js_cartClearBrn">
                        <span>Очистить корзину</span>
                    </div>
                </div>
            </div>
                {!! Form::close() !!}
        @endif
        </div>
    </div>

@endsection

