@extends('layouts.app')
@section('title', $title)
@section('content')
    @include('layouts.product.container', ['products' => $products, 'header' => $slug])
@endsection


