<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome', ['title' => 'Online-shop']);
})->name('home');

Route::get('/login', 'LoginController@show')->name('login');

Route::get('/brands', 'BrandController@show')->name('brands');

Route::get('/brands/{slug}', 'BrandProductController@show')->name('products');

Route::get('/brands/{brand}/{slug}', 'ProductController@show')->name('product');

Route::post('/searchStore', 'StoreController@searchStore')->name('searchStore');

Route::get('/store', 'StoreController@show')->name('store');

Route::post('/store/filter', 'StoreController@filter')->name('filter');

Route::post('/cart/add', 'CartController@addToCart')->name('addToCart');

Route::post('/cart/count', function () {
    return Cart::count();
});

Route::get('/cart', 'CartController@show')->name('cart');

Route::post('/cart/deleteFromCart', 'CartController@deleteFromCart')->name('deleteFromCart');

Route::post('/cart/destroyCart', 'CartController@destroyCart')->name('destroyCart');

Route::post('/cart/createOrder', 'CartController@createOrder')->name('createOrder');

Route::post('/register', 'LoginController@register')->name('register');

Route::post('/signIn', 'LoginController@signIn')->name('signIn');

Route::get('/logout', 'LoginController@logout')->name('logout');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'is.admin'], function() {
    Route::get('/', 'AdminController@show')->name('admin');
    Route::get('/orders', 'AdminController@orders')->name('admin.orders');
    Route::get('/orders/{id}', 'AdminController@orderProducts')->name('admin.orderProducts');
    Route::get('/add-brands', 'AdminController@addBrands')->name('admin.addBrands');
    Route::get('/add-products', 'AdminController@addProducts')->name('admin.addProducts');
    Route::post('/add-brands/add', 'AdminController@createBrand')->name('admin.createBrand');
    Route::post('/add-products/add', 'AdminController@createProducts')->name('admin.createProduct');
    Route::get('/orders/{id}/deliver', 'AdminController@deliver')->name('admin.deliver');
});




